<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\Ciclista;
use app\models\Puerto;
use app\models\Etapa;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        $model->password = '';
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }
    
     /*
     * Consulta 1 con DAO
     */
    public function actionConsulta1(){
        $dataProvider = new \yii\data\SqlDataProvider([
           'sql'=>'SELECT COUNT(*)as cantidadCiclistas FROM ciclista', 
        ]);
        
        return $this->render("resultado",[
           "resultados"=>$dataProvider,
           "campos"=>['cantidadCiclistas'],
           "titulo"=>"Consulta 1 con DAO",
            "enunciado"=>"Número de ciclistas que hay",
            "sql"=>"SELECT COUNT(*) AS cantidadCiclistas FROM ciclista",
        ]);
    }
    /*
     * Consulta 1 con ActiveRecord
     */
    public function actionConsulta1a(){
        $dataProvider = new \yii\data\ActiveDataProvider([
           'query' => Ciclista::find()->select("count(*) as cantidadCiclistas"),
        ]);
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['cantidadCiclistas'],
            "titulo"=>"Consulta 1 con Active Record",
            "enunciado"=>"Número de ciclistas que hay",
            "sql"=>"SELECT COUNT(*) AS cantidadCiclistas FROM ciclista",
        ]);
    }
    
     /*
     * Consulta 2 con DAO
     */
    public function actionConsulta2(){
        $dataProvider = new \yii\data\SqlDataProvider([
           'sql'=>'SELECT COUNT(*)as cantidadCiclistas FROM ciclista where nomequipo = "banesto"', 
        ]);
        
        return $this->render("resultado",[
           "resultados"=>$dataProvider,
           "campos"=>['cantidadCiclistas'],
           "titulo"=>"Consulta 2 con DAO",
            "enunciado"=>"Número de ciclistas que hay del equipo Banesto",
            "sql"=>"SELECT COUNT(*) as cantidadCiclistas FROM ciclista WHERE nomequipo = 'banesto'",
        ]);
    }
    /*
     * Consulta 2 con ActiveRecord
     */
    public function actionConsulta2a(){
        $dataProvider = new \yii\data\ActiveDataProvider([
           'query' => Ciclista::find()->select("count(*) as cantidadCiclistas")->where("nomequipo='banesto'"),
        ]);
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['cantidadCiclistas'],
            "titulo"=>"Consulta 2 con Active Record",
            "enunciado"=>"Número de ciclistas que hay del equipo Banesto",
            "sql"=>"SELECT COUNT(*) as cantidadCiclistas FROM ciclista WHERE nomequipo = 'banesto'",
        ]);
    }
    
     /*
     * Consulta 3 con DAO
     */
    public function actionConsulta3(){
        $dataProvider = new \yii\data\SqlDataProvider([
           'sql'=>'SELECT CONCAT(AVG(edad)," años") as mediaEdadCiclistas FROM ciclista', 
        ]);
        
        return $this->render("resultado",[
           "resultados"=>$dataProvider,
           "campos"=>['mediaEdadCiclistas'],
           "titulo"=>"Consulta 3 con DAO",
            "enunciado"=>"Edad media de los ciclistas",
            "sql"=>"SELECT AVG(edad) AS mediaEdadCiclistas FROM ciclista",
        ]);
    }
    /*
     * Consulta 3 con ActiveRecord
     */
    public function actionConsulta3a(){
        $dataProvider = new \yii\data\ActiveDataProvider([
           'query' => Ciclista::find()->select(['CONCAT(AVG(edad)," ", "años") AS mediaEdadCiclistas']),
        ]);
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['mediaEdadCiclistas'],
            "titulo"=>"Consulta 3 con Active Record",
            "enunciado"=>"Edad media de los ciclistas",
            "sql"=>"SELECT AVG(edad) AS mediaEdadCiclistas FROM ciclista",
        ]);
    }
    
    /*
     * Consulta 4 con DAO
     */
    
    public function actionConsulta4(){
        $dataProvider = new \yii\data\SqlDataProvider([
           'sql' => 'SELECT CONCAT(AVG(edad)," años") AS mediaEdadCiclistas FROM ciclista WHERE nomequipo = "Banesto"' 
        ]);
        
        return $this ->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['mediaEdadCiclistas'],
            "titulo"=>'Consulta 4 con DAO',
            "enunciado"=>'La edad media de los ciclistas del Banesto',
            "sql"=>'SELECT AVG(edad) AS mediaEdadCiclistas FROM ciclista WHERE nomequipo = "Banesto"',
        ]);
    }
    /*
     * Consulta 4 con active Record
     */
    public function actionConsulta4a(){
        $dataProvider = new \yii\data\ActiveDataProvider([
           'query'=> Ciclista::find()->select(['CONCAT(AVG(edad)," ", "años") AS mediaEdadCiclistas'])->where('nomequipo = "Banesto"'),
        ]);
        
        return $this->render("resultado",[
           "resultados"=>$dataProvider,
            "campos"=>['mediaEdadCiclistas'],
            "titulo"=>'Consulta 4 con ActiveRecord',
            "enunciado"=>'La edad media de los ciclistas del Banesto',
            "sql"=>'SELECT AVG(edad) AS mediaEdadCiclistas FROM ciclista WHERE nomequipo = "Banesto"',
        ]);
    }
    
    /*
     * Consulta 5 con DAO
     */
    
    public function actionConsulta5(){
        $dataProvider = new \yii\data\SqlDataProvider([
           'sql'=>'SELECT nomequipo,CONCAT(ROUND(AVG(edad),2)," años") AS mediaEdadCiclistas FROM ciclista GROUP BY nomequipo' 
        ]);
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['nomequipo','mediaEdadCiclistas'],
            "titulo"=>'Consulta 5 con DAO',
            "enunciado"=>'La edad media de los ciclistas por cada equipo',
            "sql"=>'SELECT nomequipo,CONCAT(ROUND(AVG(edad),2)," años") AS mediaEdadCiclistas FROM ciclista GROUP BY nomequipo',
        ]);
    }
    
     /*
     * Consulta 5 con active Record
     */
    public function actionConsulta5a(){
        $dataProvider = new \yii\data\ActiveDataProvider([
           'query'=> Ciclista::find()->select(['nomequipo','CONCAT(ROUND(AVG(edad),2)," años") AS mediaEdadCiclistas'])->groupBy('nomequipo'),
        ]);
        
        return $this->render("resultado",[
           "resultados"=>$dataProvider,
            "campos"=>['nomequipo','mediaEdadCiclistas'],
            "titulo"=>'Consulta 5 con ActiveRecord',
            "enunciado"=>'La edad media de los ciclistas por cada equipo',
            "sql"=>'SELECT nomequipo,CONCAT(ROUND(AVG(edad),2)," años") AS mediaEdadCiclistas FROM ciclista GROUP BY nomequipo',
        ]);
    }
    
     /*
     * Consulta 6 con DAO
     */
    
    public function actionConsulta6(){
        $dataProvider = new \yii\data\SqlDataProvider([
           'sql'=>'SELECT nomequipo, CONCAT(COUNT(*)," ciclistas") as cantidadCiclistas FROM ciclista GROUP BY nomequipo' 
        ]);
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['nomequipo','cantidadCiclistas'],
            "titulo"=>'Consulta 6 con DAO',
            "enunciado"=>'El número de ciclistas por equipo',
            "sql"=>'SELECT nomequipo, CONCAT(COUNT(*)," ciclistas") as cantidadCiclistas FROM ciclista GROUP BY nomequipo',
        ]);
    }
    
     /*
     * Consulta 6 con active Record
     */
    public function actionConsulta6a(){
        $dataProvider = new \yii\data\ActiveDataProvider([
           'query'=> Ciclista::find()->select(['nomequipo','CONCAT(COUNT(*)," ciclistas") as cantidadCiclistas'])->groupBy('nomequipo'),
        ]);
        
        return $this->render("resultado",[
           "resultados"=>$dataProvider,
            "campos"=>['nomequipo','cantidadCiclistas'],
            "titulo"=>'Consulta 6 con ActiveRecord',
            "enunciado"=>'El número de ciclistas por equipo',
            "sql"=>'SELECT nomequipo, CONCAT(COUNT(*)," ciclistas") as cantidadCiclistas FROM ciclista GROUP BY nomequipo',
        ]);
    }
    
     /*
     * Consulta 7 con DAO
     */
    
    public function actionConsulta7(){
        $dataProvider = new \yii\data\SqlDataProvider([
           'sql'=>'SELECT COUNT(*) as cantidadPuertos FROM puerto' 
        ]);
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['cantidadPuertos'],
            "titulo"=>'Consulta 7 con DAO',
            "enunciado"=>'El número total de puertos',
            "sql"=>'SELECT COUNT(*) as cantidadPuertos FROM puerto',
        ]);
    }
    
     /*
     * Consulta 7 con active Record
     */
    public function actionConsulta7a(){
        $dataProvider = new \yii\data\ActiveDataProvider([
           'query'=> Puerto::find()->select("COUNT(*) as cantidadPuertos"),
        ]);
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['cantidadPuertos'],
            "titulo"=>'Consulta 7 con ActiveRecord',
            "enunciado"=>'El número total de puertos',
            "sql"=>'SELECT COUNT(*) as cantidadPuertos FROM puerto',
        ]);
    }
    
    /*
     * Consulta 8 con DAO
     */
    
    public function actionConsulta8(){
        $dataProvider = new \yii\data\SqlDataProvider([
           'sql'=> 'SELECT COUNT(*) AS cantidadPuertos FROM puerto WHERE altura > 1500' 
        ]);
        
        return $this->render('resultado',[
            "resultados"=>$dataProvider,
            "campos"=>['cantidadPuertos'],
            "titulo"=>'Consulta 8 con dao',
            "enunciado"=>'El número total de puertos mayores de 1500',
            "sql"=>'SELECT COUNT(*) AS cantidadPuertos FROM puerto WHERE altura > 1500',
        ]);
    }
    /*
     * consulta 8 con AR
     */
    public function actionConsulta8a(){
        $dataProvider = new \yii\data\ActiveDataProvider([
           'query'=>Puerto::find()->select("COUNT(*) as cantidadPuertos")->where('altura>1500'), 
        ]);
        
        return $this->render('resultado',[
           "resultados"=>$dataProvider,
            "campos"=>['cantidadPuertos'],
            "titulo"=>'Consulta 8 con AR',
            "enunciado"=>'El número total de puertos mayores de 1500',
            "sql"=>'SELECT COUNT(*) AS cantidadPuertos FROM puerto WHERE altura > 1500',
        ]);
    }
    /*
     * consulta 9 con dao
     */
    
    public function actionConsulta9(){
        $dataProvider = new \yii\data\SqlDataProvider([
           'sql'=>'SELECT nomequipo FROM ciclista GROUP BY nomequipo HAVING COUNT(*) > 4' 
        ]);
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['nomequipo'],
            "titulo"=>"consulta 9 con dao",
            "enunciado"=>"Listar el nombre de los equipos que tengan más de 4 ciclistas",
            "sql"=>'SELECT nomequipo FROM ciclista GROUP BY nomequipo HAVING COUNT(*) > 4',
        ]);
    }
    
    public function actionConsulta9a(){
        $dataProvider = new \yii\data\ActiveDataProvider([
            'query'=> Ciclista::find()->select('nomequipo')->groupBy('nomequipo')->having('count(*)>4')
        ]);
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['nomequipo'],
            "titulo"=>"consulta 9 con Active Record",
            "enunciado"=>"Listar el nombre de los equipos que tengan más de 4 ciclistas",
            "sql"=>'SELECT nomequipo FROM ciclista GROUP BY nomequipo HAVING COUNT(*) > 4',
        ]);
    }
    
    /*
     * consulta 10 con dao
     */
    
    public function actionConsulta10(){
        $dataProvider = new \yii\data\SqlDataProvider([
            'sql'=> 'SELECT nomequipo FROM ciclista WHERE edad BETWEEN 28 AND 32 GROUP BY nomequipo HAVING COUNT(*) > 4',
        ]);
        return $this->render("resultado",[
            'resultados'=>$dataProvider,
            "campos"=>['nomequipo'],
            "titulo"=>"consulta 10 con dao",
            "enunciado"=>"Listar el nombre de los equipos que tengan más de 4 ciclistas cuya edad esté entre 28 y 32",
            "sql"=>'SELECT nomequipo FROM ciclista WHERE edad BETWEEN 28 AND 32 GROUP BY nomequipo HAVING COUNT(*) > 4',
        ]);
    }
    
    /*
     * consuta 10 con AR
     */
    
    public function actionConsulta10a(){
        $dataProvider = new \yii\data\ActiveDataProvider([
            'query'=> Ciclista::find()->select("nomequipo")->where('edad between 28 and 32')->groupBy('nomequipo')->having('count(*)>4'),
        ]);
        return $this->render("resultado",[
            'resultados'=>$dataProvider,
            "campos"=>['nomequipo'],
            "titulo"=>"consulta 10 con Active Record",
            "enunciado"=>"Listar el nombre de los equipos que tengan más de 4 ciclistas cuya edad esté entre 28 y 32",
            "sql"=>'SELECT nomequipo FROM ciclista WHERE edad BETWEEN 28 AND 32 GROUP BY nomequipo HAVING COUNT(*) > 4',
        ]);
    }
    
    /*
     * consulta 11 con dao
     */
    
    public function actionConsulta11(){
        $dataProvider = new \yii\data\SqlDataProvider([
            'sql'=>'SELECT COUNT(*) as cantidad FROM etapa GROUP BY dorsal'
        ]);
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['cantidad'],
            "titulo"=>"Consulta 11 con DAO",
            "enunciado"=>"Indícame el número de etapas que ha ganado cada uno de los ciclistas",
            "sql"=>"SELECT COUNT(*) as cantidad FROM etapa GROUP BY dorsal",
        ]);
    }
    /*
    * consulta 11 con active record
    */
    
    public function actionConsulta11a(){
        $dataProvider = new \yii\data\ActiveDataProvider([
            'query'=> Etapa::find()->select(["COUNT(*) as cantidad"])->groupBy("dorsal"),
        ]);
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['cantidad'],
            "titulo"=>"Consulta 11 con active record",
            "enunciado"=>"Indícame el número de etapas que ha ganado cada uno de los ciclistas",
            "sql"=>"SELECT COUNT(*) as cantidad FROM etapa GROUP BY dorsal",
        ]);
    }
    
        /*
     * consulta 12 con dao
     */
    
    public function actionConsulta12(){
        $dataProvider = new \yii\data\SqlDataProvider([
            'sql'=>'SELECT dorsal FROM etapa GROUP BY dorsal HAVING COUNT(*) > 1'
        ]);
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['dorsal'],
            "titulo"=>"Consulta 12 con DAO",
            "enunciado"=>"Indícame el dorsal de los ciclistas que hayan ganado más de una etapa",
            "sql"=>"SELECT dorsal FROM etapa GROUP BY dorsal HAVING COUNT(*) > 1",
        ]);
    }
    /*
    * consulta 12 con active record
    */
    
    public function actionConsulta12a(){
        $dataProvider = new \yii\data\ActiveDataProvider([
            'query'=> Etapa::find()->select("dorsal")->groupBy("dorsal")->having("COUNT(*) > 1"),
        ]);
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['dorsal'],
            "titulo"=>"Consulta 12 con active record",
            "enunciado"=>"Indícame el dorsal de los ciclistas que hayan ganado más de una etapa",
            "sql"=>"SELECT dorsal FROM etapa GROUP BY dorsal HAVING COUNT(*) > 1",
        ]);
    }
}
